package crypt

import (
	"testing"
)

func TestGenKey(t *testing.T) {
	key := GenKey("alfanum")
	println(key)
	//	fmt.Printf("\n%s\n\n", vars)
}

func TestCrypt(t *testing.T) {
	keyType := "alfas"
	cryptkey := GenKey(keyType)
	println(cryptkey)
	v := "GoTamer Crypter, light weight, very fast Data Encryption and Decryption package written in pure Go"
	println(v)
	c := GetCharacters(keyType)
	e := EnCrypt(c, cryptkey, v)
	d := DeCrypt(c, cryptkey, e)
	println(d)
}
