// The MIT License (MIT)   
// Copyright © 2012 GoTamer <http://www.robotamer.com>

// GoTamer Crypt is a light weight, very fast Data Encryption and Decryption package written in pure Go.  
// This will not pass Bank or Spy cryptographic requirements in todays world.  
// I do not make claims about its security, please see the license file for details.  
// *Advise: Please evaluate your needs and take additional measures to fit your needs. For example in my case I use multiple keys for each instance, and my application selects the keys randomly.* 
//  
// It is rune aware, it supports the full line of Go UTF-8 characters
// A UTF-8 Character List is availabe at 
// http://www.robotamer.com/utf-8.html
package crypt

import (
	"math/rand"
	"sync"
	"time"
)

const (
	alfa     = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	alfas    = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz" // with space
	num      = "0123456789"
	alfanum  = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	alfanums = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789" // with space
	url      = "#/.abcdefghijklmnopqrstuvwxyz0123456789"
	readable = "ABCDEFHJLMNQRTUVWXYZabcefghijkmnopqrtuvwxyz23479"
	uskey    = "~!@#$%^&*()_+`1234567890-=QWERTYUIOP{}|qwertyuiop[]ASDFGHJKL:asdfghjkl;'ZXCVBNM<>?zxcvbnm,./ "
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

type Crypt struct {
	mu         sync.RWMutex
	characters string
	cryptkey   string
	value      string
}

// Use the build in, or supply your own character list.
// The more characters the better.
func GetCharacters(characters string) (char string) {
	switch characters {
	case "alfa":
		char = alfa
	case "alfas":
		char = alfas
	case "num":
		char = num
	case "alfanum":
		char = alfanum
	case "alfanums":
		char = alfanums
	case "url":
		char = url
	case "readable":
		char = readable
	case "uskey":
		char = uskey
	default:
		char = characters
	}
	return
}

// Generates a Private Crypt Key 
func GenKey(characters string) string {
	char := GetCharacters(characters)
	return shuffle(char)
}

func EnCrypt(characters, cryptkey, value string) string {
	c := new(Crypt)
	c.mu.Lock()
	defer c.mu.Unlock()
	c.characters = characters
	c.cryptkey = cryptkey
	c.value = value
	return c.enCrypt()
}

func DeCrypt(characters, cryptkey, value string) string {
	c := new(Crypt)
	c.mu.Lock()
	defer c.mu.Unlock()
	c.characters = characters
	c.cryptkey = cryptkey
	c.value = value
	return c.deCrypt()
}

func (c *Crypt) enCrypt() (out string) {
	crs := []rune(c.characters)
	krs := []rune(c.cryptkey)
	vrs := []rune(c.value)
	for _, v := range vrs {
		i := findKey(v, krs)
		if i == -1 {
			out += string(v)
		} else {
			out += string(crs[i])
		}
	}
	return
}

func (c *Crypt) deCrypt() (out string) {
	crs := []rune(c.characters)
	krs := []rune(c.cryptkey)
	vrs := []rune(c.value)
	for _, v := range vrs {
		i := findKey(v, crs)
		if i == -1 {
			out += string(v)
		} else {
			out += string(krs[i])
		}
	}
	return
}

func findKey(v rune, rs []rune) (i int) {
	for p, r := range rs {
		if r == v {
			i = p
			return
		}
	}
	return -1
}

// Shuffles the string
// from "bitbucket.org/gotamer/tools"
func shuffle(s string) string {
	l := len(s)
	b := []byte(s)
	for i := len(b) - 1; i != 0; i-- {
		r := rand.Intn(l)
		b[i], b[r] = b[r], b[i]
	}
	return string(b)
}
